package com.reconnect;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Sending a request to the site and processing the response
 */
public class Connect {
    /**
     * Get user data by id
     * @param user id to request information
     */
    public static void setupConnection(int user) {

        String id = Integer.toString(user);
        String urlAdress = "https://reqres.in/api/users/" + id;
        URL url;
        HttpURLConnection httpURLConnection;
        int responseCode;
        String inputLine;

        try {
            url = new URL(urlAdress);
            httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.addRequestProperty("User-Agent", "Mozilla/5.0");
            httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setConnectTimeout(1000);
            httpURLConnection.setReadTimeout(1000);
            httpURLConnection.connect();

            responseCode = httpURLConnection.getResponseCode();

            if (responseCode == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                JSONParser jsonParser = new JSONParser();
                JSONObject jsonObject = (JSONObject) jsonParser.parse(String.valueOf(response));
                JSONObject now = (JSONObject) jsonParser.parse(String.valueOf(jsonObject.get("data")));

                System.out.println(now.get("first_name")+ " " + now.get("last_name"));
            } else if (responseCode == 404) {
                System.out.println("User not found!");
            } else {
                System.out.println("Request failed, please try again later.");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.err.print(e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
