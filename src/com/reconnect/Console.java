package com.reconnect;

import java.util.Scanner;

/**
 * Input processing
 */
public class Console {
    /**
     * We get the user id and check its validity
     *
     * @return the number entered into the console
     */
    public static int inputConsole() {

        Scanner scan = new Scanner(System.in);
        boolean flag = true;
        int value = 0;

        System.out.print("java reconnect ");

        while (flag) {
            while (!scan.hasNextInt()) {
                System.out.println("Incorrect id value entered, please try again.");
                scan.next();
            }

            value = scan.nextInt();
            if (value > 0) {
                flag = false;
            } else {
                System.out.println("Incorrect id value entered, please try again.");
            }
        }

        scan.close();
        return value;
    }
}
