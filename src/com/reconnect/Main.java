package com.reconnect;

/**
 * Main application class
 */
public class Main {
    /**
     * Method that starts the process of searching for the user's first and last name by id
     * @param args array of console line arguments
     */
    public static void main(String[] args) {

        Connect.setupConnection(Console.inputConsole());
    }
}
